const plays = deps => {
  return {
    listAll () {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query(
          'select * from plays order by id desc',
          (error, results) => {
            if (error)
              return errorHandler(error, 'Erro ao listar as jogadas', reject)
            return resolve(JSON.parse(JSON.stringify(results)))
          }
        )
      })
    },
    save (data) {
      return new Promise((resolve, reject) => {
        const { connection, errorHandler } = deps
        connection.query(
          'INSERT INTO plays (play, description) VALUES(?, ?)',
          [data.play, data.description],
          (error, results) => {
            if (error)
              return errorHandler(error, 'Erro ao salvar as jogadas', reject)
            return resolve(JSON.parse(JSON.stringify(results)))
          }
        )
      })
    }
  }
}

module.exports = plays
