let board
const player = 'O'
const AI = 'X'
let level =
  localStorage.getItem('level') || localStorage.setItem('level', 1) || 1
const winCombos = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [6, 4, 2]
]

const cells = [...document.querySelectorAll('.cell')]

const selectDifficulty = () => {
  const difficulty = document.getElementById('difficulty')

  if (localStorage.getItem('level')) {
    difficulty.options[parseInt(localStorage.getItem('level'))].setAttribute(
      'selected',
      'selected'
    )
  }

  if (difficulty) {
    difficulty.addEventListener(
      'change',
      event => {
        if (parseInt(event.target.value) === 1) {
          localStorage.setItem('level', 1)
          level = 1
        } else if (parseInt(event.target.value) === 2) {
          localStorage.setItem('level', 2)
          level = 2
        } else {
          localStorage.setItem('level', 3)
          level = 3
        }
      },
      false
    )
  }
}

/**
 *Template
 */

const render = () => {
  axios
    .get('plays')
    .then(response => {
      if (response.data.length === 0) {
        templatelistPlays('Histórico Vazio...')
        return
      }
      for (const post of response.data) {
        templatelistPlays(post.play)
      }
    })
    .catch(error => {
      console.error(error)
      templatelistPlays(
        'Estamos com problemas para se comunicar com o servidor, mais fique tranquilo já estamos resolvendo !!'
      )
    })
}

const hideListPlays = () => {
  let app = document.getElementById('plays')
  let links = [...document.getElementsByTagName('a')]
  for (let link of links) {
    app.removeChild(link)
  }
}

const templatelistPlays = play => {
  let a = document.createElement('a')
  let app = document.getElementById('plays')

  a.setAttribute('class', 'panel-block')
  a.textContent = play || ''
  app.appendChild(a)
}

const checkWinner = (board, player) => {
  let plays = board.reduce((acc, value, idx) => {
    return value === player ? acc.concat(idx) : acc
  }, [])
  let gameWon = null
  for (const [index, win] of winCombos.entries()) {
    if (win.every(elem => plays.indexOf(elem) > -1)) {
      gameWon = { index, player }
      break
    }
  }
  return gameWon
}

const gameOver = wonGame => {
  for (let index of winCombos[wonGame.index]) {
    document.getElementById(index).style.backgroundColor =
      wonGame.player === player ? 'cyan' : 'red'
  }

  for (let cell of cells) {
    cell.removeEventListener('click', turnClick, false)
  }

  declareWinner(wonGame.player === player ? 1 : 0)
  hideListPlays()
  render()
}

const turnClick = event => {
  if (typeof board[event.target.id] === 'number') {
    turn(event.target.id, player)
    /**
     * Nivel 1 - 2 - 3
     * */
    if (!checkTie()) turn(bestSpot(), AI)
  }
}

/**
 * Verifica se houve empate
 */
const checkTie = () => {
  if (emptySquares().length === 0) {
    for (const cell of cells) {
      cell.style.backgroundColor = 'yellow'
      cell.removeEventListener('click', turnClick, false)
    }
    declareWinner(2)
    return true
  }

  return false
}

/**
 * Retorna um array com todos os locais vazios
 */
const emptySquares = () => {
  return board.filter(s => typeof s === 'number')
}

const bestSpot = () => {
  if (level === 1) {
    return emptySquares()[0]
  } else if (level === 2) {
    return levelNormal(emptySquares()[0])
  } else {
    return levelHard(emptySquares()[0])
  }
}

const declareWinner = who => {
  /**
   * 0 - Você perdeu
   * 1 - Você Ganhou
   * 2 -  Empate
   */
  axios
    .post('/plays', {
      play:
        who === 0
          ? `${localStorage.getItem('nome') || 'Player'} Perdeu`
          : who === 1
          ? `${localStorage.getItem('nome') || 'Player'} Ganhou`
          : 'Empate',
      description: 'teste'
    })
    .then(response => {
      Swal.fire({
        icon: who === 0 ? 'error' : who === 1 ? 'success' : 'warning',
        title:
          who === 0
            ? `Que pena ${localStorage.getItem('nome') ||
                'Player'} Você Perdeu =(`
            : who === 1
            ? `Parabéns ${localStorage.getItem('nome') ||
                'Player'} Você Ganhou =)`
            : 'Empate !'
      })
    })
}

const turn = (elementID, player) => {
  board[elementID] = player
  document.getElementById(elementID).innerText = player
  const wonGame = checkWinner(board, player)
  if (wonGame) gameOver(wonGame)
}

const startGame = () => {
  board = Array.from(Array(9).keys())
  for (const cell of cells) {
    cell.innerText = ''
    cell.style.removeProperty('background-color')
    cell.addEventListener('click', turnClick, false)
  }
}

const changeName = () => {
  localStorage.clear()
  document.location.reload(true)
}

if (localStorage.getItem('nome')) {
  startGame()
} else {
  Swal.fire({
    input: 'text',
    inputPlaceholder: 'Coloque seu nome:'
  }).then(nome => {
    if (nome.value === undefined) {
      localStorage.setItem('nome', 'Player')
    }
    localStorage.setItem('nome', nome.value)
    startGame()
  })
}

const init = () => {
  selectDifficulty()
  hideListPlays()
  render()
}

init()

const levelNormal = randomSlot => {
  /**
   * Horizontal
   */
  /** {0 - 1 - 2} */
  if (board[0] === 'O' && board[1] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 1 - 0} */
  if (board[2] === 'O' && board[1] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 2 - 1} */
  if (board[0] === 'O' && board[2] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {2 - 0 - 1} */
  if (board[2] === 'O' && board[0] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {3 - 4 - 5} */
  if (board[3] === 'O' && board[4] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /** {5 - 4 - 3} */
  if (board[5] === 'O' && board[4] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {3 - 5 - 4} */
  if (board[3] === 'O' && board[5] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {5 - 3 - 4} */
  if (board[5] === 'O' && board[3] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {6 - 7 - 8} */
  if (board[6] === 'O' && board[7] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 7 - 6} */
  if (board[8] === 'O' && board[7] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 8 - 7} */
  if (board[6] === 'O' && board[8] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** {8 - 6 - 7} */
  if (board[8] === 'O' && board[6] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** Vertical */
  /** {0 - 3 - 6} */
  if (board[0] === 'O' && board[3] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 3 - 0} */
  if (board[6] === 'O' && board[3] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 6 - 3} */
  if (board[0] === 'O' && board[6] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {6 - 0 - 3} */
  if (board[6] === 'O' && board[0] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {1 - 4 - 7} */
  if (board[1] === 'O' && board[4] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** {7 - 4 - 1} */
  if (board[7] === 'O' && board[4] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {1 - 7 - 4} */
  if (board[1] === 'O' && board[4] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {7 - 1 - 4} */
  if (board[7] === 'O' && board[1] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {2 - 5 - 8} */
  if (board[2] === 'O' && board[5] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 5 - 2} */
  if (board[8] === 'O' && board[5] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 8 - 5} */
  if (board[2] === 'O' && board[8] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /** {8 - 2 - 5} */
  if (board[8] === 'O' && board[2] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /**
   * Diagonal
   */
  /** {0 - 4 - 8} */
  if (board[0] === 'O' && board[4] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 4 - 0} */
  if (board[8] === 'O' && board[4] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 8 - 4} */
  if (board[0] === 'O' && board[8] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {8 - 0 - 4} */
  if (board[8] === 'O' && board[0] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {2 - 4 - 6} */
  if (board[2] === 'O' && board[4] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 4 - 2} */
  if (board[6] === 'O' && board[4] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 6 - 4} */
  if (board[2] === 'O' && board[6] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {6 - 2 - 4} */
  if (board[6] === 'O' && board[2] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  return randomSlot
}

const levelHard = randomSlot => {
  /**
   * Horizontal
   */
  /** {0 - 1 - 2} */
  if (board[0] === 'O' && board[1] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 1 - 0} */
  if (board[2] === 'O' && board[1] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 2 - 1} */
  if (board[0] === 'O' && board[2] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {2 - 0 - 1} */
  if (board[2] === 'O' && board[0] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {3 - 4 - 5} */
  if (board[3] === 'O' && board[4] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /** {5 - 4 - 3} */
  if (board[5] === 'O' && board[4] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {3 - 5 - 4} */
  if (board[3] === 'O' && board[5] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {5 - 3 - 4} */
  if (board[5] === 'O' && board[3] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {6 - 7 - 8} */
  if (board[6] === 'O' && board[7] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 7 - 6} */
  if (board[8] === 'O' && board[7] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 8 - 7} */
  if (board[6] === 'O' && board[8] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** {8 - 6 - 7} */
  if (board[8] === 'O' && board[6] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** Vertical */
  /** {0 - 3 - 6} */
  if (board[0] === 'O' && board[3] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 3 - 0} */
  if (board[6] === 'O' && board[3] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 6 - 3} */
  if (board[0] === 'O' && board[6] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {6 - 0 - 3} */
  if (board[6] === 'O' && board[0] === 'O' && typeof board[3] !== 'string') {
    return 3
  }

  /** {1 - 4 - 7} */
  if (board[1] === 'O' && board[4] === 'O' && typeof board[7] !== 'string') {
    return 7
  }

  /** {7 - 4 - 1} */
  if (board[7] === 'O' && board[4] === 'O' && typeof board[1] !== 'string') {
    return 1
  }

  /** {1 - 7 - 4} */
  if (board[1] === 'O' && board[4] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {7 - 1 - 4} */
  if (board[7] === 'O' && board[1] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {2 - 5 - 8} */
  if (board[2] === 'O' && board[5] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 5 - 2} */
  if (board[8] === 'O' && board[5] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 8 - 5} */
  if (board[2] === 'O' && board[8] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /** {8 - 2 - 5} */
  if (board[8] === 'O' && board[2] === 'O' && typeof board[5] !== 'string') {
    return 5
  }

  /**
   * Diagonal
   */
  /** {0 - 4 - 8} */
  if (board[0] === 'O' && board[4] === 'O' && typeof board[8] !== 'string') {
    return 8
  }

  /** {8 - 4 - 0} */
  if (board[8] === 'O' && board[4] === 'O' && typeof board[0] !== 'string') {
    return 0
  }

  /** {0 - 8 - 4} */
  if (board[0] === 'O' && board[8] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {8 - 0 - 4} */
  if (board[8] === 'O' && board[0] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {2 - 4 - 6} */
  if (board[2] === 'O' && board[4] === 'O' && typeof board[6] !== 'string') {
    return 6
  }

  /** {6 - 4 - 2} */
  if (board[6] === 'O' && board[4] === 'O' && typeof board[2] !== 'string') {
    return 2
  }

  /** {2 - 6 - 4} */
  if (board[2] === 'O' && board[6] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  /** {6 - 2 - 4} */
  if (board[6] === 'O' && board[2] === 'O' && typeof board[4] !== 'string') {
    return 4
  }

  return randomSlot
}
