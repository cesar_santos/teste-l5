const path = require('path')
require('dotenv').config({ path: path.resolve(__dirname, '..', '.env') })
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()

const PORT = process.env.PORT || 3000

const db = require(path.resolve(__dirname, 'database', 'mysql'))

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.static(path.resolve(__dirname, 'public')))

/**
 * API
 */
app.get('/plays', async (req, res) => {
  try {
    const plays = await db.plays.listAll()
    res.json(plays)
  } catch (error) {
    console.error(error)
    res.status(500).json('Erro ao lista as jogadas')
  }
})

app.post('/plays', async (req, res) => {
  try {
    await db.plays.save(req.body)
    res.status(201).json(req.body)
  } catch (error) {
    console.error(error)
    res.status(500).json('Erro ao salvar a jogada')
  }
})

app.listen(PORT, () => {
  console.info(`server running on port ${PORT}`)
})
